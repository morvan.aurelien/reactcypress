import {sizes} from "../../fixtures/sizes";

describe('Use cases on login page', () => {
    beforeEach(() => {
        localStorage.clear();
        cy.visit('/se-connecter');
    })
    sizes.forEach((size) => {

        it(`Check if login button is disabled if no identifier or no password filled ${size}`, () => {
            cy.viewport(size);
            cy.get('[data-testid=button-login]').should('have.attr', 'disabled');
        })

        it(`Check if login button is enabled if identifier & password filled : ${size}`, () => {
            cy.viewport(size)
            cy.get('input[data-testid=input-identifier]').type('johndoe');
            cy.get('input[data-testid=input-password]').type('123456');
            cy.get('[data-testid=button-login]').should('have.not.attr', 'disabled');
        })

        it(`Submit login form with invalid credentials : ${size}`, () => {
            cy.viewport(size)
            // eslint-disable-next-line no-undef
            cy.intercept('POST', '/auth', (req) => {
                req.reply({
                    statusCode: 401,
                    fixture: 'login/invalid.json'
                })
            })
            cy.get('input[data-testid=input-identifier]').type('johndoe')
            cy.get('input[data-testid=input-password]').type('123456');
            cy.get('[data-testid=button-login]').click();
            cy.get('body').should('contain', 'Invalid credentials')
        })

        it(`Successful login : ${size}`, () => {
            cy.viewport(size)
            cy.intercept('POST', '/auth', (req) => {
                req.reply({
                    statusCode: 200,
                    fixture: 'login/valid.json',
                    delay: 1000
                })
            })
            cy.get('input[data-testid=input-identifier]').type('johndoe')
            cy.get('input[data-testid=input-password]').type('123456');
            cy.get('[data-testid=button-login]').click();
            cy.get('[data-testid=loader]').should('be.visible');
            cy.location('pathname').should('eq', '/')
            cy.store().its('security').its('token').should('eq', 'untoken')
            // cy.window().its('store').invoke('getState').its('security').its('token').should('eq', 'untoken');
        })

        it(`Try access login page with auth user : ${size}`, () => {
            cy.viewport(size)
            cy.intercept('POST', '/auth', (req) => {
                req.reply({
                    statusCode: 200,
                    fixture: 'login/valid.json'
                })
            })
            cy.get('input[data-testid=input-identifier]').type('johndoe')
            cy.get('input[data-testid=input-password]').type('123456');
            cy.get('[data-testid=button-login]').click();
            cy.visit('/se-connecter')
            cy.location('pathname').should('eq', '/')
        })
    })
})
