describe('use cases on homepage', () => {
    beforeEach(() => {
        localStorage.clear();
    })

    it('access homepage without login and check redirect to login page', () => {
        cy.visit('/');
        cy.url().should('contain', '/se-connecter')
        cy.get('[data-testid=input-identifier]').should('be.visible')
        cy.get('[data-testid=input-password]').should('be.visible')
    })
})
