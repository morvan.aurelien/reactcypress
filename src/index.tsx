import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from "react-redux";
import {persistoreFct, storeCustom} from "./redux";
import {PersistGate} from "redux-persist/integration/react";
import {Loader} from "./components/Loader";

import './i18n/index';

ReactDOM.render(
  <React.StrictMode>
      <Provider store={storeCustom}>
          <PersistGate persistor={persistoreFct(storeCustom)} loading={null}>
              <Loader />
              <App />
          </PersistGate>
      </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
// @ts-ignore
if (window.Cypress) {
    // @ts-ignore
    window.store = storeCustom;
}

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
