import React  from 'react';
import {Button, Grid, makeStyles, Typography} from "@material-ui/core";
import {Header} from "../components/Header";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        marginTop: theme.spacing(1),
    }
}));

const Home = () => {
    const classes = useStyles();
    return(
        <div>
            <Header />
            <Grid container spacing={2} className={classes.root}>
                <Grid item xs={12}>
                    <Typography variant="body1">
                        Page d'accueil
                    </Typography>
                </Grid>
            </Grid>
        </div>
    );
}

export default Home;
