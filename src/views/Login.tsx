import React, {useState} from 'react';
import {BackgroundImage} from "../components/BackgroundImage";
import {Button, Grid, makeStyles, Typography} from "@material-ui/core";
import {GenericCard} from "../components/Cards/GenericCard";
import {TextInput} from "../components/FormsInput/TextInput";
import {Send} from "@material-ui/icons";
import {useDispatch, useSelector} from "react-redux";
import {login} from "../redux/actions/security";

const useStyles = makeStyles((theme) => ({
    container_card: {
        position: "absolute",
        top: "40%",
        transform: "translate(-50%, -40%)",
        left: "50%",
        padding: 10,

    },
    form_wrapper: {
        padding: 20
    },
}));

export const Login = () => {
    const classes = useStyles();
    const [identifier, setIdentifier] = useState('');
    const [password, setPassword] = useState('');
    const errorApi = useSelector((state: any) => state.app.errorApi)
    const dispatch = useDispatch();
    const handleLogin = () => {
        dispatch(login(identifier, password))
    }

    return (
        <BackgroundImage>
            <GenericCard
                classContainer={classes.container_card}
                content={
                    <Grid container spacing={3} className={classes.form_wrapper}>
                        {
                            errorApi && <Grid item xs={12}><Typography>{errorApi}</Typography></Grid>
                        }
                        <Grid item xs={12} md={6}>
                            <TextInput
                                id="identifier"
                                label="Identifiant"
                                testId="input-identifier"
                                required
                                handleChange={setIdentifier}
                            />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <TextInput
                                id="password"
                                label="Mot de passe"
                                testId="input-password"
                                type="password"
                                required
                                handleChange={setPassword}
                            />
                        </Grid>
                        <Grid item xs={12} style={{textAlign: 'center'}}>
                            <Button
                                variant="contained"
                                color="primary"
                                endIcon={<Send />}
                                onClick={handleLogin}
                                data-testid="button-login"
                                disabled={identifier.trim().length === 0 || password.trim().length === 0}
                            > Se connecter
                            </Button>
                        </Grid>
                    </Grid>
                }>
            </GenericCard>
        </BackgroundImage>
    )
}
