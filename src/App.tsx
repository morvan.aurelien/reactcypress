import React from 'react';
import './App.css';
import {
    Switch,
} from 'react-router-dom';
import Home from './views/Home';
import { Login } from './views/Login';
import {GuardedRoute, GuardProvider} from "react-router-guards";
import {notRequiredLogin, requiredLogin} from "./guards/required-login";
import {ConnectedRouter} from "connected-react-router";
import {history} from "./redux";
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#E77B54"
        },
        secondary: {
            main: '#367BFE'
        }
    }
})

function App() {
  return (
      <ThemeProvider theme={theme}>
          <ConnectedRouter history={history}>
              <GuardProvider>
                  <Switch>
                      <GuardedRoute path="/se-connecter" exact component={Login} guards={[notRequiredLogin]} meta={{no_auth: true}} />
                      <GuardedRoute path="/" exact component={Home} guards={[requiredLogin]} meta={{auth: true}} />
                  </Switch>
              </GuardProvider>
          </ConnectedRouter>
      </ThemeProvider>
  );
}

export default App;
