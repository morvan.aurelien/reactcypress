import {storeCustom} from "../redux";

export const requiredLogin = (to: any, from: any, next: any) => {
    to.meta.auth ?
        isLogin() ? next() : next.redirect('/se-connecter') :
        next();
}

export const notRequiredLogin = (to: any, from: any, next: any) => {
    if (to.meta.no_auth) {
        if (isLogin() && to.location.pathname === '/se-connecter') {
            next.redirect(from.location.pathname !== to.location.pathname ? from.location.pathname : '/')
        }
        next();
    } else {
        next();
    }
}

export const isLogin = () => {
    const state = storeCustom.getState();

    return state.security.token !== null;
}
