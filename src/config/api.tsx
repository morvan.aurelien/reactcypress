const BASE_API = 'http://localhost:8000';

const basicHeaders = () => {
    return {'Content-Type': 'application/json'}
}

export const POST = (uri: string, body: string, additionnalHeaders: any = {}) => {
    const requestOptions = {
        method: 'POST',
        headers: {basicHeaders, ...additionnalHeaders},
        body: body
    }
    return fetch(`${BASE_API}${uri}`, requestOptions)
        .then(handleResponse)
}

const handleResponse = (response: Response) => {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                // location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}
