export const listRoutes = [
    {name: 'homepage', to: '/', label: 'Accueil'},
    {name: 'search', to: '/recherche', label: 'Recherche'},
    {name: 'login', to: '/se-connecter', label: 'Connexion'},
]
