import React from "react";
import {mount} from "@cypress/react";
import {TextInput} from "../../../components/FormsInput/TextInput";

it('Test renderer basic input', () => {
    //@ts
    mount(<TextInput id="Test-id" label="test label" testId="Test"/>)
    cy.get('div[data-testid=Test] > label').should('contain.text', 'test label')
})

it('Test renderer standard variant input', () => {
    mount(<TextInput id="Test-id" testId="Test" variant="standard"/>)
    cy.get('div[data-testid=Test] > label').should('not.exist')
    cy.get('div[data-testid=Test]').trigger('mouseover');
})
