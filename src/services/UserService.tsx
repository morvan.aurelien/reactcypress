import {handleResponse} from "../api/base";
import {AUTH} from "../api/ENDPOINTS";

export const UserService = {
    login
}

function login(username: string, password: string) {
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({username, password})
    }
    return fetch(`http://localhost:8000${AUTH}`, requestOptions)
        .then(handleResponse);
}
