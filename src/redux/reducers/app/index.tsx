import {ERROR_API, TOGGLE_LOADING} from "../../actions/app/types";

export interface AppState {
    loading: boolean;
    errorApi: string | null
}

const initialState: AppState = {
    loading: false,
    errorApi: null
}

export const app = (state: AppState = initialState, action: any) => {
    let nextState;
    switch (action.type) {
        case TOGGLE_LOADING:
            nextState = {
                ...state,
                loading: !state.loading
            }
            break;
        case ERROR_API:
            nextState = {
                ...state,
                errorApi: action.message
            }
            break;
        default:
            return state;
    }

    return nextState;
}
