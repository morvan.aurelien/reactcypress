import {LOGIN, LOGOUT} from "../../actions/security/types";

export interface SecurityState {
    token: string|null;
}

const initialState: SecurityState = {
    token: null
}

export const security = (state: SecurityState = initialState, action: any) => {
    let nextState;
    switch (action.type) {
        case LOGIN:
            nextState = {
                ...state,
                token: action.value
            }
            break;
        case LOGOUT:
            nextState = {
                ...state,
                token: null
            }
            break;
        default:
            return state;
    }

    return nextState;
}
