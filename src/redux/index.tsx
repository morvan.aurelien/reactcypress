import storage from "redux-persist/lib/storage"
import {createMigrate, persistReducer, persistStore} from 'redux-persist';
import {applyMiddleware, combineReducers, createStore, Store} from "redux";
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";
import {app} from "./reducers/app";
import {security} from "./reducers/security";
import {connectRouter, routerMiddleware} from "connected-react-router";
import { createBrowserHistory } from 'history'

export const history = createBrowserHistory()

const migrations = {
    1: (state: any) => ({
        ...state,
    })
}

const persistConfig = {
    key: 'root',
    storage,
    blacklist: ['app'],
    version: 1,
    migrate: createMigrate(migrations, {debug: true}),
}

const reducers = (history: any) => combineReducers({
    app: app,
    security: security,
    router: connectRouter(history)
});

const persistedReducer = persistReducer(persistConfig, reducers(history));

export const storeCustom: Store = createStore(
    persistedReducer,
    composeWithDevTools(applyMiddleware(routerMiddleware(history), thunk)),
);

export const persistoreFct = (store: Store) => {
    return persistStore(store);
};

