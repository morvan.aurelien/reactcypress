import {ERROR_API, TOGGLE_LOADING} from "./types";
import {push} from "connected-react-router";

export interface IToggleLoading {}
export interface IErrorApi {
    type: string,
    message: string|null
}

export const toggleLoading = () => {
    return (dispatch: any) => {
        const action: IToggleLoading = {type: TOGGLE_LOADING};
        dispatch(action);
    }
}

export const redirect = (url: string) => {
    return (dispatch: any) => {
        dispatch(push(url))
    }
}

export const errorApi = (error: string|null) => {
    return (dispatch: any) => {
        const action: IErrorApi = {type: ERROR_API, message: error}
        dispatch(action);
    }
}
