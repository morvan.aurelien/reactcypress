import {errorApi, redirect, toggleLoading} from "../app";
import {UserService} from "../../../services/UserService";
import {LOGIN, LOGOUT} from "./types";
import {listRoutes} from "../../../config/routes";

export const login = (identifier: string, password: string) => {
    return (dispatch: any) => {
        dispatch(toggleLoading());
        UserService.login(identifier, password)
            .then(data => {
                const action = {type: LOGIN, value: data.token}
                dispatch(action);
                dispatch(toggleLoading());
                dispatch(errorApi(null))
                const routeFiltered = listRoutes.filter(item => item.name === 'homepage')[0];
                dispatch(redirect(routeFiltered.to))
            })
            .catch(err => {
                dispatch(toggleLoading());
                dispatch(errorApi(err))
            })
    }
}

export const logout = () => {
    return (dispatch: any) => {
        dispatch({type: LOGOUT});
        const routeFiltered = listRoutes.filter(item => item.name === 'login')[0];
        dispatch(redirect(routeFiltered.to));
    }
}
