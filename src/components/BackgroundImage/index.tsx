import React from 'react'
import {Container, makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
    container: {
        backgroundImage: 'url(./assets/images/bkg_login.jpg)',
        height: '100vh',
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "center"
    }
})

export const BackgroundImage = (props: any) => {
    const classes = useStyles();
    return (
        <Container maxWidth={false} className={classes.container}>
            {props.children}
        </Container>
    )
}
