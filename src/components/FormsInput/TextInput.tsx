import React from "react";
import {makeStyles, TextField} from "@material-ui/core";
import clsx from "clsx";

const useStyles = makeStyles(() => ({
    input: {
        width: "100%",
    },
}))

export const TextInput = (props: any) => {
    const classes = useStyles(props);

    return (
        <TextField
            id={props.id}
            className={clsx(classes.input, props.classInput)}
            data-testid={props.testId}
            label={props.label}
            variant={props.variant ?? 'outlined'}
            value={props.value}
            inputProps={{
                'data-testid': props.testId
            }}
            type={props.type ?? 'text'}
            size={props.smallInput ?? 'medium'}
            onChange={(e) => props.handleChange(e.target.value)}
            required={props.required}
        />
    )
}
