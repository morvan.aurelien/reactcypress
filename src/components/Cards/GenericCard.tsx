import React from 'react';
import {Card, CardActions, CardContent, CardHeader, makeStyles} from "@material-ui/core";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
    container: {
        minWidth: 310,
        boxShadow: "5px 5px 15px 5px #000000",
    }
}));

export const GenericCard = (props: any) => {
    const classes = useStyles();
    const _renderHeader = () => {
        return <CardHeader>
            {props.header}
        </CardHeader>
    }

    const _renderContent = () => {
        return <CardContent>
            {props.content}
        </CardContent>
    }

    const _renderActions = () => {
        return <CardActions>
            {props.actions}
        </CardActions>
    }

    return(
        <Card className={clsx(classes.container, props.classContainer)}>
            {props.header ? _renderHeader() : null }
            {props.content ? _renderContent() : null}
            {props.actions ? _renderActions() : null}
        </Card>
    )
}
