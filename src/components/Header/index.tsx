import {AppBar, Button, IconButton, makeStyles, Toolbar, Typography} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import React from "react";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {logout} from "../../redux/actions/security";
import {ExitToAppRounded, LockRounded} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(1),
    },
    title: {
        flexGrow: 1,
    },
    link_first: {
        marginRight: theme.spacing(3)
    }
}));

export const Header = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton edge="start" className={classes.menuButton} color="secondary" aria-label="menu">
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" color="secondary" className={classes.title}>
                    Homepage
                </Typography>
                <Link to="/se-connecter" className={classes.link_first}>
                    <LockRounded color="secondary" />
                </Link>
                <IconButton onClick={() => dispatch(logout())} edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                    <ExitToAppRounded color="secondary" />
                </IconButton>
            </Toolbar>
        </AppBar>
    )
}
