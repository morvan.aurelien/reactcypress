import React, {createRef} from "react";
import {useSelector} from "react-redux";
import {Backdrop, CircularProgress, makeStyles} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    }
}))

export const Loader = (props: any) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const loading = props.loading ?? useSelector((state: any) => state.app.loading);
    const classes = useStyles();
    const ref = createRef();

    return (
        <Backdrop data-testid="loader" className={classes.backdrop} open={loading} ref={ref}>
            <CircularProgress color="inherit" />
        </Backdrop>
    )
}
